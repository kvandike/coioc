# README #

This README would normally document whatever steps are necessary to get your application up and running.

# **What is this repository for?** #

CoIoC is an IoC container. It manages the dependencies between classes so that applications stay easy to change as they grow in size and complexity.

# **Getting Started** #

You can get CoIoC via [nuget](https://www.nuget.org/packages/CoIoC/).

# **Basic features** #

Basic features that run out of the box.

## *Create container* ##

Default settings (basic methods).

```
#!c#
var container = new BasicContainer();
```


or custom settings



```
#!c#
var builder = new ContainerBuilder();
var container = builder
      .ToAdapter(new BasicMappingAdapter())
      .AddScanner(new BasicScanner(ScanAssembly, "scan.namespace"))
      .Build();
```


## *Register component* ##

* ### *Manual* ###

Standard registration.


```
#!c#
var container = new BasicContainer();
container.Register<ISimpleService,SimpleService>();
//in this situation are not implemented in the components of variables and properties at object initialization
container.Register<IOtherSimpleService>(() => new OtherSimpleService());
// OtherSimpleService need implement IOtherSimpleService (resolve correct)
container.Register(() => new OtherSimpleService());
```


* ### *Auto* ###

Contains all classes relating to [Attributes](https://msdn.microsoft.com/ru-ru/library/system.attribute(v=vs.110).aspx) of **[Service]** or **[Controller]**.


```
#!c#
var builder = new ContainerBuilder();
var container = builder
      .AddScanner(new BasicScanner(ScanAssembly, "scan.namespace"))
      .AddScanner(new BasicScanner(OtherScanAssembly, "other.scan.namespace"))
      .Build();
container.Scan();
```


## *Resolve component* ##

Pull the component from the container.


```
#!c#
var simpleService = container.Resolve<ISimpleService>();
var specificSimpleService = container.Resolve<ISimpleService>("specificName");
```


# **Features** #

CoIoC supports the following features.

* **[Autowire]** - This attribute applies to variables and properties. Using this attribute, you do not need to worry about how best to convey the class component of the container. CoIoC find the desired component and substitute its value in a property or variable attribute is noted.

* **[Qualifier(Name = "specificName")]** - There may be a situation when you create more than one component of the same type and want to wire only one of them with a property, in such case you can use **[Qualifier]** annotation along with **[Autowired]** to remove the confusion by specifying which exact component will be wired.

* **[ConstructorInject]** - When the target class contains more than one designer, you want to apply the attribute **[ConstructorInject]** to the designer to specify CoIoC constructor to initialize the object.

* **[Service]** - Attribute classes at service layer level. Required when scanning to detect Assembly.

* **[Controller]** - Attribute classes at controller layer level. Required when scanning to detect Assembly.

There are ways that you need to initialize the component and to initialize its variables and properties of the components from the container.
You can do this as follows.

```
#!c#

public class OtherController : IOtherController
{
public ISimpleService SimpleService { get;set; }
}


//Somewhere
void Init(){
var builder = new ContainerBuilder();
var container = builder.Build();
container.Register<ISimpleService,SimpleService>();
var otherController = new OtherController();
otherController.SimpleService = container.Resolve<ISimpleService>();
}
```

But the following approach is the most concise.

```
#!c#

public class OtherController : IOtherController
{
public ISimpleService SimpleService { get;set; }
}


//Somewhere
void Init(){
var builder = new ContainerBuilder();
var container = builder.Build();
container.Register<ISimpleService,SimpleService>();
var otherController = new OtherController();
container.InjectComponents(otherController);
}
```
**You must mark variables and properties attributes [Autowire] ([Qualifier]).*

## *Example* ##

* ### Example with two classes that implement and registered under a single interface. ###

```
#!c#
public interface ISimpleService{ }

[Service]
public class SimpleService : ISimpleService{ }

[Service("specificName")]
public class SpecificSimpleService : ISimpleService{ }

public class Application {
   void Main(){
   var container = new BasicContainer();
   container.Register<ISimpleService,SimpleService>();
   container.Register<ISimpleService,SpecificSimpleService>();
   var simpleService = container.Resolve<ISimpleService>();
   var specificSimpleService = container.Resolve<ISimpleService>("specificName");
   }
}

```
* ### Example using [Autowire], [Qualifier], [ConstructorInject]. ###


```
#!c#

//Base interface
public interface ISimpleService { }

[Service]
public SimpleService : ISimpleService { }

[Service("specificName")]
public SpecificSimpleService : ISimpleService { }

[Service]
public class OtherSimpleService: IOtherSimpleService {

   private _secondSimpleService;

   [Autowire]
   public ISimpleService SimpleService { get; set; }

   [Autowire]
   [Qualifier("specificName")]
   private ISimpleService SpecificSimpleService;

   //initialization by default, if [ConstructorInject] is not set
   public OtherSimpleService()
   {
   }

   [ConstructorInject]
   public OtherSimpleService(ISecondSimpleService secondSimpleService)
   {
   _secondSimpleService = secondSimpleService;
   }
}

//Somewhere
void Init(){
var container = new BasicContainer();
container.Register<ISimpleService,SimpleService>();
container.Register<ISimpleService,SpecificSimpleService>();
container.Register<ISecondSimpleService,SecondSimpleService>();
container.Register<IOtherSimpleService,OtherSimpleService>();
var otherSimpleService = container.Resolve<IOtherSimpleService>();
var specificSimpleService = otherSimpleService.SpecificSimpleService;
}

```
# **Customization?** #

* ## *Scanner* ##

By default, you can add the scanner base **BasicScanner**, but you can also implement and add your scanner to automatically detect components. You must implement the **IScanner** interface and add it.

```
#!c#

public class CustomScanner : IScanner 
{ 
//implement methods
}

//Somewhere
void Init() { 
var builder = new ContainerBuilder();
var container = builder
      //base scanner
      .AddScanner(new BasicScanner(ScanAssembly, "scan.namespace"))
      //custom scanner
      .AddScanner(new CustomScanner(OtherScanAssembly, "other.scan.namespace"))
      .Build();
container.Scan();
}
```

* ## *Inject* ##

Add the default base injection: **BasicInject**. You can also implement and add an implementation of injections 
***(basic injection is not added when adding a custom!)***. 
You must implement the interface IInjec and add it.


```
#!c#

public class CustomInject : IInject
{
//implement methods
}

//Somewhere
void Init() { 
var builder = new ContainerBuilder();
var container = builder
      .AddInject(new CustomInject())
      .Build();
}
```

* ## *Mapping Adapter* ##

The adapter is used to store the initialized objects. The default **BasicMappingAdapter**. To add your adapter must implement the interface **IMappingAdapter** and add it.



```
#!c#

public class CustomMappingAdapter : IMappingAdapter
{
//implement methods
}

//Somewhere
void Init() { 
var builder = new ContainerBuilder();
var container = builder
      .ToAdapter(new CustomMappingAdapter())
      .Build();
}
```