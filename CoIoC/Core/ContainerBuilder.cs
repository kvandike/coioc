﻿using System.Collections.Generic;
using CoIoC.Interface;

namespace CoIoC.Core
{
    public sealed class ContainerBuilder
    {
        private IMappingAdapter _adapter;
        private IList<IScanner> _scanners;
        private IList<IInject> _injects;

        public ContainerBuilder ToAdapter(IMappingAdapter adapter)
        {
            _adapter = adapter;
            return this;
        }

        public ContainerBuilder AddScanner(IScanner scanner)
        {
            if (_scanners == null)
                _scanners = new List<IScanner>();
            _scanners.Add(scanner);
            return this;
        }

        public ContainerBuilder AddInject(IInject inject)
        {
            if (_injects == null)
                _injects = new List<IInject>();
            _injects.Add(inject);
            return this;
        }

        public Container Build()
        {
            var mapperObjects = _adapter == null
                ? new MappingObjects(new BasicMappingAdapter())
                : new MappingObjects(_adapter);
            if (_scanners != null)
            {
                mapperObjects.Scanners.AddRange(_scanners);
            }
            if (_injects == null)
                _injects = new List<IInject> {new BasicInject()};
            mapperObjects.Injects.AddRange(_injects);
            var container = new Container(mapperObjects);
            return container;
        }
    }
}
