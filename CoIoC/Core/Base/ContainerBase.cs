﻿using System;
using CoIoC.Interface;

namespace CoIoC.Core.Base
{
    public abstract class ContainerBase : IContainer
    {
        internal MappingObjectsBase MappingObjects;

        internal ContainerBase(MappingObjectsBase mappingObjects)
        {
            MappingObjects = mappingObjects;
        }

        public abstract void Register<TContract, TImplement>() where TImplement : TContract;
        public abstract void Register<TImplement>(Func<TImplement> lazyInit);
        public abstract TContract Resolve<TContract>(string name = null);
    }
}
