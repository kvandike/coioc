﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CoIoC.Attr;
using CoIoC.Interface;

namespace CoIoC.Core.Base
{
    /// <summary>
    /// single map
    /// </summary>
    internal abstract class MappingObjectsBase
    {
        private readonly IMappingAdapter _adapter;
        private List<IInject> _injects;
        private List<IScanner> _scanners;


        public List<IScanner> Scanners
        {
            get { return _scanners ?? (_scanners = new List<IScanner>()); }
        }

        public List<IInject> Injects
        {
            get { return _injects ?? (_injects = new List<IInject>()); }
        }


        protected MappingObjectsBase(IMappingAdapter adapter)
        {
            _adapter = adapter;
        }

        #region Add Contract

        public void AddObject<TImplement>(Func<TImplement> lazyInit)
        {
            var bean = typeof (TImplement).GetTypeInfo().GetCustomAttribute<Bean>();
            var contract = typeof (TImplement);
            contract = FindContract(contract);
            _adapter.Register(contract, new Lazy<object>(() =>
            {
                var init = lazyInit.Invoke();
                InjectComponents(init);
                return init;
            }), bean != null ? bean.Name : null);
        }

        public void AddObject<TContract, TImplement>()
        {
            var bean = typeof (TImplement).GetTypeInfo().GetCustomAttribute<Bean>();
            CreatedLazyObject(typeof (TContract), typeof (TImplement), bean != null ? bean.Name : null);
        }

        public void AddObject(Type contract, Type implement)
        {
            var bean = implement.GetTypeInfo().GetCustomAttribute<Bean>();
            CreatedLazyObject(contract, implement, bean != null ? bean.Name : null);
        }


        #endregion


        #region Get Implement

        public TContract GetObject<TContract>(string name = null)
        {
            return (TContract) GetObject(typeof (TContract), name);
        }

        public object GetObject(Type contract, string name = null)
        {
            contract = FindContract(contract);
            var lazyInit = _adapter.GetObject(contract, name);
            return lazyInit == null ? null : lazyInit;
        }

        #endregion



        /// <summary>
        /// Create Lazy Object
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="implement"></param>
        /// <param name="name"></param>
        protected void CreatedLazyObject(Type contract, Type implement, string name = null)
        {
            contract = FindContract(contract);
            Func<object> func = () =>
            {
                var init = CreateForType(implement);
                InjectComponents(init);
                return init;
            };
            var lazyInit = new Lazy<object>(func);
            _adapter.Register(contract, lazyInit, name);
        }

        /// <summary>
        /// Create Object fo Type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected object CreateForType(Type type)
        {
            var typeInfo = type.GetTypeInfo();
            if (typeInfo.DeclaredConstructors.Count() == 1)
                return CreateForConstructorInfo(type, typeInfo.DeclaredConstructors.ElementAt(0));
            foreach (
                var constructor in
                    typeInfo.DeclaredConstructors.Where(
                        constructor => constructor.GetCustomAttribute<ConstructorInject>() != null))
            {
                return CreateForConstructorInfo(type, constructor);
            }
            throw new NullReferenceException(string.Format("not create object {0}", type.FullName));
        }


        /// <summary>
        /// Create Object For ConstructorType
        /// </summary>
        /// <param name="type"></param>
        /// <param name="constructor"></param>
        /// <returns></returns>
        protected object CreateForConstructorInfo(Type type, ConstructorInfo constructor)
        {
            var constructorParameters = constructor.GetParameters();
            if (constructorParameters.Length == 0)
            {
                return Activator.CreateInstance(type);
            }
            var parameters = new List<object>(constructorParameters.Length);
            parameters.AddRange(
                constructorParameters.Select(parameterInfo => GetObject(parameterInfo.ParameterType)));
            return constructor.Invoke(parameters.ToArray());
        }


        /// <summary>
        /// Find Contract Interface
        /// </summary>
        /// <param name="contract"></param>
        /// <returns></returns>
        private Type FindContract(Type contract)
        {
            var typeInfo = contract.GetTypeInfo();
            if (typeInfo.IsInterface) return contract;
            var firstInterface = typeInfo.ImplementedInterfaces.FirstOrDefault();
            return firstInterface ?? contract;
        }


        public void InjectComponents(object init)
        {
            if (_injects == null) return;
            var pullComponent = new BasicPullComponent(GetObject);
            foreach (var inject in _injects)
            {
                inject.Inject(pullComponent, init);
            }
        }

        /// <summary>
        /// Scan Assembly {Service, Controller}
        /// </summary>
        public void Scan()
        {
            if (_scanners == null) return;
            foreach (var scanner in _scanners)
            {
                var listTypeInfo = scanner.Scan();
                foreach (var typeInfo in listTypeInfo)
                {
                    var typeContract = (typeInfo.ImplementedInterfaces == null || !typeInfo.ImplementedInterfaces.Any()) ? typeInfo.AsType() : typeInfo.ImplementedInterfaces.ElementAt(0);
                    var typeImplement = typeInfo.AsType();
                    AddObject(typeContract, typeImplement);
                }
            }
        }
    }
}
