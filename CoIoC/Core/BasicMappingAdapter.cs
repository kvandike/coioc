﻿using System;
using System.Collections.Generic;
using CoIoC.Interface;

namespace CoIoC.Core
{
    public class BasicMappingAdapter : IMappingAdapter
    {
        private  IDictionary<Tuple<Type, string>, Lazy<object>> _mappingObjects;

        protected IDictionary<Tuple<Type, string>, Lazy<object>> MappingObjects
        {
            get { return _mappingObjects ?? (_mappingObjects = new Dictionary<Tuple<Type, string>, Lazy<object>>()); }
        }

        public object GetObject(Type contract, string name = null)
        {
            var key = Tuple.Create(contract, name);
            return MappingObjects.ContainsKey(key) ? MappingObjects[key].Value : null;
        }


        public bool ContainsContract(Type contract, string name = null)
        {
            return MappingObjects.ContainsKey(Tuple.Create(contract, name));
        }

        public void Register(Type contract, Lazy<object> implement, string name = null)
        {
            MappingObjects[Tuple.Create(contract, name)] = implement;
        }
    }
}
