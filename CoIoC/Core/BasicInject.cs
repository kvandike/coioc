﻿using System.Linq;
using System.Reflection;
using CoIoC.Attr;
using CoIoC.Core.Base;
using CoIoC.Interface;

namespace CoIoC.Core
{
    public class BasicInject : IInject
    {
        public void Inject(IPullComponent pullComponent, object init)
        {
            var typeInfo = init.GetType().GetTypeInfo();

            //field
            foreach (
                var declaredField in
                    typeInfo.DeclaredFields.Where(x => x.GetCustomAttribute<Autowire>() != null))
            {
                var qualifier = declaredField.GetCustomAttribute<Qualifier>();
                string name = null;
                if (qualifier != null)
                    name = qualifier.Name;
                var fieldType = declaredField.FieldType;
                var fieldValue = pullComponent.Pull(fieldType, name);
                declaredField.SetValue(init, fieldValue);
            }

            //property
            foreach (
                var declaredProperty in
                    typeInfo.DeclaredProperties.Where(x => x.CanWrite && x.GetCustomAttribute<Autowire>() != null))
            {
                var qualifier = declaredProperty.GetCustomAttribute<Qualifier>();
                string name = null;
                if (qualifier != null)
                    name = qualifier.Name;
                var propertyType = declaredProperty.PropertyType;
                var propertyValue = pullComponent.Pull(propertyType, name);
                declaredProperty.SetValue(init, propertyValue);
            }
        }
    }
}
