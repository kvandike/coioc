﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CoIoC.Attr;
using CoIoC.Interface;

namespace CoIoC.Core
{
    public class BasicScanner : IScanner
    {
        public BasicScanner(Assembly assembly, string scanNamespace = null)
        {
            ScanNamespace = string.IsNullOrEmpty(scanNamespace) ? string.Empty : scanNamespace;
            Assembly = assembly;
        }

        public string ScanNamespace { get;}
        public Assembly Assembly { get;}

        public IList<TypeInfo> Scan()
        {
            return
                Assembly.DefinedTypes.Where(x => x.IsClass && !x.IsAbstract && x.GetCustomAttribute<Bean>() != null && x.Namespace.Contains(ScanNamespace))
                    .ToList();
        }
    }
}
