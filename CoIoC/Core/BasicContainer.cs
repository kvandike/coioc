﻿using System;
using CoIoC.Core.Base;

namespace CoIoC.Core
{
    public class BasicContainer : ContainerBase
    {
        private BasicContainer(MappingObjectsBase mappingObjects) : base(mappingObjects)
        {
        }

        public BasicContainer() : this(new MappingObjects(new BasicMappingAdapter()))
        {
            MappingObjects.Injects.Add(new BasicInject());
        }

        public override void Register<TContract, TImplement>()
        {
            MappingObjects.AddObject<TContract, TImplement>();
        }


        public override void Register<TImplement>(Func<TImplement> lazyInit)
        {
            MappingObjects.AddObject(lazyInit);
        }

        public override TImplement Resolve<TImplement>(string name = null)
        {
            return MappingObjects.GetObject<TImplement>(name);
        }

    }
}
