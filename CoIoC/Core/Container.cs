﻿using System;
using CoIoC.Core.Base;

namespace CoIoC.Core
{
    public class Container : ContainerBase
    {

        internal Container(MappingObjectsBase mappingObjects) : base(mappingObjects)
        {
        }

        public void Scan()
        {
            MappingObjects.Scan();
        }

        public void InjectComponents(object init)
        {
            MappingObjects.InjectComponents(init);
        }

        public override void Register<TContract, TImplement>()
        {
            MappingObjects.AddObject<TContract, TImplement>();
        }

        public override void Register<TImplement>(Func<TImplement> lazyInit)
        {
            MappingObjects.AddObject(lazyInit);
        }


        public override TImplement Resolve<TImplement>(string name = null)
        {
            return MappingObjects.GetObject<TImplement>(name);
        }
    }
}
