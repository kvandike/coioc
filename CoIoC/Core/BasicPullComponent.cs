﻿using System;
using CoIoC.Interface;

namespace CoIoC.Core
{
    public class BasicPullComponent : IPullComponent
    {
        private readonly Func<Type, string,object> _actionAdapter;

        public BasicPullComponent(Func<Type, string, object> actionAdapter)
        {
            _actionAdapter = actionAdapter;
        }


        public object Pull(Type contract, string name = null)
        {
            return _actionAdapter(contract, name);
        }
    }
}
