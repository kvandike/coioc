﻿using System;

namespace CoIoC.Attr
{
    /// <summary>
    /// if a lot of constructors, the use
    /// </summary>
    [AttributeUsage(AttributeTargets.Constructor)]
    public class ConstructorInject : Attribute
    {
    }
}
