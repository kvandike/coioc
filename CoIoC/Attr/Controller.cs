﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoIoC.Attr
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Controller : Bean
    {
        public Controller()
        {
        }

        public Controller(string name) : base(name)
        {
        }
    }
}
