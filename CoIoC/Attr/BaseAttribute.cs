﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoIoC.Attr
{
    public abstract class BaseAttribute : Attribute
    {
        protected BaseAttribute()
        {
        }

        protected BaseAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

    }
}
