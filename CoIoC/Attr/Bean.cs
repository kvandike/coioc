﻿using System;

namespace CoIoC.Attr
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class Bean : BaseAttribute
    {
        public Bean()
        {
        }

        public Bean(string name) : base(name)
        {
        }
    }
}
