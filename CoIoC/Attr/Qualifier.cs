﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoIoC.Attr
{

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class Qualifier : BaseAttribute
    {
        public Qualifier()
        {
        }

        public Qualifier(string name) : base(name)
        {
        }
    }
}
