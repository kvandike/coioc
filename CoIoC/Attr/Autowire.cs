﻿using System;

namespace CoIoC.Attr
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class Autowire :Attribute
    {
    }
}
