﻿using System;

namespace CoIoC.Attr
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Service : Bean
    {
        public Service(string name) : base(name)
        {
        }

        public Service()
        {
        }
    }
}
