﻿using System;

namespace CoIoC.Interface
{
    public interface IPullComponent
    {
        object Pull(Type contract, string name = null);
    }
}
