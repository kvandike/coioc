﻿namespace CoIoC.Interface
{
    public interface IInject
    {
        void Inject(IPullComponent pullComponent, object init);
    }
}
