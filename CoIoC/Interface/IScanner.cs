﻿using System.Collections.Generic;
using System.Reflection;

namespace CoIoC.Interface
{
    public interface IScanner
    {

        IList<TypeInfo> Scan();
    }
}
