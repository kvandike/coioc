﻿using System;

namespace CoIoC.Interface
{
    public interface IMappingAdapter
    {
        object GetObject(Type contract, string name = null);

        bool ContainsContract(Type contract, string name = null);

        void Register(Type contract, Lazy<object> implement, string name = null);
    }
}
