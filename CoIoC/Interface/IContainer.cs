﻿using System;

namespace CoIoC.Interface
{
    public interface IContainer
    {
        void Register<TContract, TImplement>() where TImplement : TContract;

        void Register<TImplement>(Func<TImplement> lazyInit);

        TContract Resolve<TContract>(string name = null);

    }
}
