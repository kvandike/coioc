﻿using System;

namespace CoIoC.Ext
{
    public class ReflectObject
    {
        public Lazy<object> Init { get; private set; }


        public ReflectObject(Func<object> init)
        {
            Init = new Lazy<object>(init);
        }
    }
}
