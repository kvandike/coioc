﻿using System;
using System.Collections.Generic;
using System.Reflection;
using CoIoC.Core;
using CoIoC.Interface;
using CoIoC.Test.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoIoC.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void BuilderTest()
        {
            var assembly = GetType().GetTypeInfo().Assembly;
            var builder = new ContainerBuilder();
            var container = builder
                .ToAdapter(new BasicMappingAdapter())
                .AddInject(new BasicInject())
                .AddScanner(new BasicScanner(assembly))
                .Build();
            container.Scan();
            var parent = container.Resolve<IParentSimpleService>();
        }

        //[TestMethod]
        public void TestMethod1()
        {

            var container = new BasicContainer();
            container.Register<IASimpleService,ASimpleService>();
            container.Register<IBSimpleService,BSimpleService>();
            //container.Register<IBSimpleService,BNonSimpleService>();
            //container.Register<IChildSimpleService,ChildSimpleService>();
           // container.Register<IParentSimpleService>(() => new ParentSimpleService());
           container.Register<IParentSimpleService,ParentSimpleService>();
            var parent = container.Resolve<IParentSimpleService>();
            var parent1 = container.Resolve<IParentSimpleService>();
            var parent2 = container.Resolve<IParentSimpleService>();

        }

        //[TestMethod]
        public void BeanInjectComponents()
        {
            var builder = new ContainerBuilder();
            var container = builder.Build();
            container.Register<IBSimpleService,BSimpleService>();
            container.Register<IBSimpleService,BNonSimpleService>();
            container.Register<IChildSimpleService,ChildSimpleService>();
            var parentService = new ParentSimpleService();
            container.InjectComponents(parentService);
        }

        //[TestMethod]
        public void TwoBeanBuild()
        {
            var container = new BasicContainer();
            container.Register<IBSimpleService, BSimpleService>();
            container.Register<IBSimpleService, BNonSimpleService>();
            //var service = container.Resolve<IBSimpleService>("bname");
        }


    }
}
