﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoIoC.Attr;

namespace CoIoC.Test.Service
{
    [Attr.Service(Name = "bname")]
    public class BSimpleService : IBSimpleService
    {
        public string b = "hello name";
    }
}
