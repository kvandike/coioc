﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoIoC.Attr;

namespace CoIoC.Test.Service
{
    [Attr.Service]
    public class ChildSimpleService : IChildSimpleService
    {
        private string name = "i child";

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

    }
}
