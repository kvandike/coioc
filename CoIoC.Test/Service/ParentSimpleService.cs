﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoIoC.Attr;

namespace CoIoC.Test.Service
{
    [Controller]
    public class ParentSimpleService: IParentSimpleService
    {
        [Autowire]
        public IChildSimpleService ChildSimpleService;

        [Autowire]
        [Qualifier(Name = "aname")]
        public IASimpleService SimpleService { get; set; }

        [Autowire]
        [Qualifier(Name = "bname")]
        private IBSimpleService BSimpleService;

        [Autowire]
        private IBSimpleService BNonSimpleService;

       
        public ParentSimpleService()
        {
            
        }

        [ConstructorInject]
        public ParentSimpleService(IChildSimpleService childSimpleService)
        {
            ChildSimpleService = childSimpleService;
        }



    }
}
